package javaschool;

import com.querydsl.sql.MySQLTemplates;
import com.querydsl.sql.SQLQuery;
import com.querydsl.sql.SQLTemplates;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

public class JDBCExample {

    public static void main(String[] args) {

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        Connection conn = null;
        try {
            conn = DriverManager.getConnection("jdbc:mysql://localhost/javaschool?user=root&password=root");

            QPerson person = QPerson.Person;

            SQLTemplates dialect = new MySQLTemplates();

            SQLQuery<?> query = new SQLQuery<Void>(conn, dialect);

            List<String> names = query.select(person.name)
                    .from(person)
                    .where(person.id.goe(1))
                    .fetch();

            System.out.println(names);
        } catch (SQLException e) {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                e.printStackTrace();
            }
        }
    }
}
